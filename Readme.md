# AssemblyInfoPatcher

[![Build status](https://ci.appveyor.com/api/projects/status/7xcjyfgmp62ms4rf/branch/master?svg=true)](https://ci.appveyor.com/project/theAdmiral777/assemblyinfopatcher/branch/master)

A simple console application to update the AssemblyInfo.cs file in your C# project. This is so your built and deployed files have their file version set for easy version checking. Run this before your build so that it builds with the specified version.

## Requirements
- .Net 4.5
- Nuget

## Usage
`AssemblyInfoPatcher.exe -file="C:\YourRepo\YourProject\Properties\AssemblyInfo.cs" -version="1.2.3"`

For a more dynamic approach (e.g. from your build server):

`AssemblyInfoPatcher.exe -file="C:\YourRepo\YourProject\Properties\AssemblyInfo.cs" -version=%version%`

where %version% is set by the build server.