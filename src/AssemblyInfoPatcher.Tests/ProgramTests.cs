﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyInfoPatcher.Tests
{
    class ProgramTests
    {
        IEnumerable<TestCaseData> Args_Testing_TestCaseData()
        {
            // file not specified
            string[] args = {};
            yield return new TestCaseData(args, typeof(ArgumentException), "specify a file").SetName("No file specified");

            // version not specified
            args = new string[] { "-file=\"C:\\somefile\"" };
            yield return new TestCaseData(args, typeof(ArgumentException), "specify a version").SetName("No version specified");

        }

        [Test]
        [TestCaseSource("Args_Testing_TestCaseData")]
        public void Main_TestingArgs_Failure(string[] args, Type expectedExceptionType, string exceptionMessage)
        {
            try
            {
                Program.Main(args);
            }
            catch(Exception e)
            {
                Assert.IsInstanceOf(expectedExceptionType, e, "The wrong exception type was thrown");
                Assert.IsTrue(e.Message.Contains(exceptionMessage), "The exception message did not contain the correct text");
                return;
            }
            Assert.Fail("An exception was expected but none were thrown");
        }
    }
}
