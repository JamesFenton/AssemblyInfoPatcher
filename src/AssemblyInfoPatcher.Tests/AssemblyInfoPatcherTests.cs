﻿using AssemblyInfoPatcher.Tests.Properties;
using NUnit.Framework;
using System;
using System.IO.Abstractions.TestingHelpers;

namespace AssemblyInfoPatcher.Tests
{
    [TestFixture]
    class AssemblyInfoPatcherTests
    {
        [Test]
        public void PatchToVersion_Patches_Correctly()
        {
            var expected = Resources.UpdatedAssemblyInfo;
            var patcher = new AssemblyInfoPatcher(new MockFileSystem());
            var actual = patcher.PatchToVersion(Resources.AssemblyInfo, "1.2.3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PatchToVersion_MissingVersionToken_ThrowsException()
        {
            var patcher = new AssemblyInfoPatcher(new MockFileSystem());
            
            try
            {
                var actual = patcher.PatchToVersion(Resources.AssemblyInfo___no__version_, "1.2.3");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOf<InvalidOperationException>(e, "The wrong exception type was thrown");
                Assert.IsTrue(e.Message.Contains("does not contain the token {version}"), "The exception message did not contain the correct text");
                return;
            }
            Assert.Fail("An exception was expected but none were thrown");
        }

        [Test]
        public void PatchFileToVersion_Works_Correctly()
        {
            string filePath = "C:\\some file path\\AssemblyInfo.cs";
            var expected = Resources.UpdatedAssemblyInfo;

            var inputFile = Resources.AssemblyInfo;
            var mockFileSystem = new MockFileSystem();
            mockFileSystem.AddFile(filePath, inputFile);

            var patcher = new AssemblyInfoPatcher(mockFileSystem);
            patcher.PatchFileToVersion(filePath, "1.2.3");

            var actual = mockFileSystem.File.ReadAllText(filePath);

            Assert.AreEqual(expected, actual);
        }
    }
}
