﻿using AssemblyInfoPatcher.IoC;
using Autofac;
using NDesk.Options;
using System;
using System.Collections.Generic;

namespace AssemblyInfoPatcher
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // IoC
            var container = InitIoC();

            // parse arguments
            bool help = false;
            string file = null;
            string version = null;
            var p = new OptionSet
            {
                { "file=",      v => file = v },
                { "version=", v => version = v },
                { "h|?|help",   v => help = v != null },
            };
            List<string> extras = p.Parse(args);

            // check arguments
            if (file == null)
                throw new ArgumentException("Please specify a file to patch with: -file=\"PathToAssemblyInfo.cs\"");
            if (version == null)
                throw new ArgumentException("Please specify a version to patch with: -version=\"1.0.1\"");

            // start processing
            using (var scope = container.BeginLifetimeScope())
            {
                var patcher = scope.Resolve<AssemblyInfoPatcher>();
                patcher.PatchFileToVersion(file, version);
                Console.WriteLine("The file {0} was updated to version {1}", file, version);
            }
        }

        static IContainer InitIoC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<MainModule>();
            return builder.Build();
        }
    }
}
