﻿using Autofac;
using System.IO.Abstractions;

namespace AssemblyInfoPatcher.IoC
{
    class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileSystem>().As<IFileSystem>();
            builder.RegisterType<AssemblyInfoPatcher>();
        }
    }
}
