﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyInfoPatcher
{
    /// <summary>
    /// The object that patches assembly info.
    /// </summary>
    public class AssemblyInfoPatcher
    {
        /// <summary>
        /// The token that must be present in the AssemblyInfo.cs file. 
        /// eg: [assembly: AssemblyVersion("{version}")]
        /// </summary>
        public const string TokenToReplaceVersionWith = "{version}";

        readonly IFileSystem _fileSystem;

        public AssemblyInfoPatcher(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;
        }

        /// <summary>
        /// Updates an AssemblyInfo.cs file contents to have the specified <paramref name="version"/>.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        public void PatchFileToVersion(string filePath, string version)
        {
            var originalFile = _fileSystem.File.ReadAllText(filePath);
            var updatedFile = PatchToVersion(originalFile, version);
            _fileSystem.File.WriteAllText(filePath, updatedFile);
        }

        /// <summary>
        /// Updates an AssemblyInfo.cs file contents to have the specified <paramref name="version"/>.
        /// </summary>
        /// <param name="assemblyInfoVersionFileContents">The file contents of the AssemblyInfo.cs file</param>
        /// <param name="version">The version to patch to</param>
        /// <returns>The file contents of the updated AssemblyInfo.cs file</returns>
        public string PatchToVersion(string assemblyInfoVersionFileContents, string version)
        {
            if (!assemblyInfoVersionFileContents.Contains(TokenToReplaceVersionWith))
                throw new InvalidOperationException("The specified file does not contain the token {version}\n" +
                                                    "Please ensure your AssemblyInfo.cs contains:\n" +
                                                    "[assembly: AssemblyVersion(\"{version}\")]\n" +
                                                    "[assembly: AssemblyFileVersion(\"{version}\")]");
            var updatedFileContents = assemblyInfoVersionFileContents.Replace(TokenToReplaceVersionWith, version);
            return updatedFileContents;
        }
    }
}
